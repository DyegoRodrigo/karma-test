/**
* Created by Arthur on 1/9/15.
*/
var trs = [];
var x = document.getElementsByTagName("tr");

for (var i = 0; i <x.length; i++)
    trs.push(x[i]);

// This for is used when you want to compare parts of the Suite based on a text symbol. Eventually, it might be a good idea to teste with substrings and splits to see if the same may be accomplished with less code.
/*for (var i = 0; i < trs[4].cells[2].innerText.length; i++)
    if(trs[4].cells[2].innerText[i] == ">")
    {
        previousSuite = previousSuite.substring(0, previousSuite.length - 1);
        break;
    }
    else
    {
        previousSuite += trs[4].cells[2].innerText[i];
    }*/

var suite = "";
var previousSuite = trs[4].cells[2].innerText;

for(i = 5; i < trs.length; i++) {
    suite = trs[i].cells[2].innerText;

    // This for is used when you want to compare parts of the Suite based on a text symbol. Eventually, it might be a good idea to teste with substrings and splits to see if the same may be accomplished with less code.
    /*
    for (var j = 0; j < trs[i].cells[2].innerText.length; j++)
        if (trs[i].cells[2].innerText[j] == ">") {
            suite = suite.substring(0, suite.length - 1);
            break;
        }
        else
            suite += trs[i].cells[2].innerText[j];
            */

    if (suite != previousSuite) {
        var newTestFileRow = trs[i];

        var header = document.getElementsByClassName('header');


        var headerClone;
        headerClone = header[0].cloneNode(true);
        headerClone.id += "x";

        newTestFileRow.parentNode.insertBefore(headerClone, newTestFileRow);

        previousSuite = suite;


    }
}